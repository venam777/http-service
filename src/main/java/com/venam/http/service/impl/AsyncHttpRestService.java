package com.venam.http.service.impl;

import com.venam.http.service.api.*;
import com.venam.http.service.util.ExceptionableCallback;
import org.asynchttpclient.*;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Http-сервис с использованием библиотеки org.asynchttpclient<br/>
 * Так как реализуется интерфейс HttpService, данный сервис умеет отправлять как синхронные, так и асинхронные запросы<br/>
 * Пример использования:<br/>
 * <pre>
 * {@code
 * HttpRequest request = new HttpRequest.Builder()
 *                 .setBasicAuthorization("JSW", "123456")
 *                 .setType(HttpMethodType.GET)
 *                 .setHost("http://srv-app-jsd.bft.local")
 *                 .setPort(8090)
 *                 .setUrlPath("/rest/api/2/issue/SDTT-50/")
 *                 .build();
 *         try {
 *             httpService.sendSyncRequest(request);
 *         } catch (HttpRequestException e) {
 *             //log.error
 *         }
 *       }
 * </pre>
 * Документация по библиотеке: https://www.baeldung.com/async-http-client
 */
public class AsyncHttpRestService implements HttpService {

    private HttpConnectionParams defaultConnectionParams = new HttpConnectionParams.Builder()
            .setConnectionTimeout(5 * 1000).setRequestTimeout(5 * 1000).build();
    private ExecutorService executorService = Executors.newCachedThreadPool();

    //private static final Logger logger = LoggerManager.getLogger(AsyncHttpRestService.class);

    @Override
    public HttpResponse sendSyncRequest(HttpRequest request) throws HttpRequestException {
        return sendSyncRequest(request, defaultConnectionParams);
    }

    @Override
    public HttpResponse sendSyncRequest(HttpRequest request, HttpConnectionParams connectionParams) throws HttpRequestException {
        Request req = buildRequest(request);
        try (AsyncHttpClient httpClient = buildHttpClient(connectionParams)) {
            Future<Response> executor = httpClient.executeRequest(req);
            return convertResponse(executor.get());
        } catch (InterruptedException e) {
            throw new HttpRequestException("Http request execution was interrupted ", e);
        } catch (ExecutionException e) {
            throw new HttpRequestException("Error while executing http request ", e);
        } catch (IOException e) {
            throw new HttpRequestException("Error while closing http client ", e);
        }
    }

    @Override
    public void sendAsyncRequest(HttpRequest request, ExceptionableCallback<HttpResponse, HttpRequestException> callback) {
        sendAsyncRequest(request, defaultConnectionParams, callback);
    }

    @Override
    public void sendAsyncRequest(HttpRequest request, HttpConnectionParams connectionParams, ExceptionableCallback<HttpResponse, HttpRequestException> runnable) {
        Request req = buildRequest(request);
        AsyncHttpClient httpClient = buildHttpClient(connectionParams);
        ListenableFuture<Response> executor = httpClient.executeRequest(req);
        executor.addListener(() -> {
            try {
                if (runnable != null) {
                    try {
                        runnable.run(convertResponse(executor.get()));
                    } catch (InterruptedException e) {
                        runnable.onException(new HttpRequestException("Http request execution was interrupted ", e));
                    } catch (ExecutionException e) {
                        runnable.onException(new HttpRequestException("Error while executing http request ", e));
                    }
                }
            } finally {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    if (runnable != null) {
                        runnable.onException(new HttpRequestException("Error while closing http client ", e));
                    }
                }
            }

        }, executorService);
    }

    @Override
    public void setDefaultConnectionParams(HttpConnectionParams connectionParams) {
        this.defaultConnectionParams = connectionParams;
    }

    private Request buildRequest(HttpRequest request) {
        RequestBuilder requestBuilder;
        switch (request.getType()) {
            case GET:
                requestBuilder = Dsl.get(request.getUrl());
                break;
            case POST:
                requestBuilder = Dsl.post(request.getUrl());
                break;
            case PUT:
                requestBuilder = Dsl.put(request.getUrl());
                break;
            case DELETE:
                requestBuilder = Dsl.delete(request.getUrl());
                break;
            default:
                throw new IllegalArgumentException("Unsupported http request type: " + (request.getType() != null ? request.getType().name() : "null"));
        }
        Map<String, String> headerParams = request.getHeaders();
        if (headerParams != null) {
            for (String key : headerParams.keySet()) {
                requestBuilder.addHeader(key, headerParams.get(key));
            }
        }
        //todo доделать
//        List<HttpRequestBodyPart> bodyParts = request.getBodyParts();
//        if (bodyParts != null) {
//            for (HttpRequestBodyPart part : bodyParts) {
//                switch (part.getPartType()) {
//                    case STRING:
//                        requestBuilder.addBodyPart(new StringPart(part.getName(), (String) part.getContent(), part.getContentType(), part.getCharset()));
//                        break;
//                    case FILE:
//                        requestBuilder.addBodyPart(new FilePart(part.getName(), (File) part.getContent(), part.getContentType(), part.getCharset()));
//                        break;
//                }
//            }
//        }
        HttpRequestBody body = request.getBody();
        if (body != null) {
            switch (body.getType()) {
                case FILE:
                    requestBuilder.setBody((File) body.getContent());
                    break;
                case STRING:
                    requestBuilder.setBody((String) body.getContent());
                    break;
            }
        }
        return requestBuilder.build();
    }

    private AsyncHttpClient buildHttpClient(HttpConnectionParams connectionParams) {
        return Dsl.asyncHttpClient(getHttpClientConfig(connectionParams));
    }

    private HttpResponse convertResponse(Response response) {
        return new HttpResponse.DefaultImpl(response.getStatusCode(), response.getStatusText(), response.getResponseBody());
    }

    private AsyncHttpClientConfig getHttpClientConfig(HttpConnectionParams connectionParams) {
        DefaultAsyncHttpClientConfig.Builder builder = Dsl.config();
        if (connectionParams.getConnectionTimeout() != null) {
            builder.setConnectTimeout(connectionParams.getConnectionTimeout());
        }
        if (connectionParams.getRequestTimeout() != null) {
            builder.setRequestTimeout(connectionParams.getRequestTimeout());
        }
        return builder.build();
    }
}
