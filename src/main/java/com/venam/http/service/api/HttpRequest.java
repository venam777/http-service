package com.venam.http.service.api;

import org.json.JSONObject;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * Http-Запрос. Имеет встроенный Builder для конфигурирования
 */
public interface HttpRequest {

    String getUrl();

    HttpMethodType getType();

    Map<String, String> getHeaders();

    HttpRequestBody getBody();

    //List<HttpRequestBodyPart> getBodyParts();

    class DefaultImpl implements HttpRequest {

        private String url;
        private HttpMethodType type;
        private Map<String, String> headers;
        private HttpRequestBody body;

        public DefaultImpl(String url, HttpMethodType type, Map<String, String> headers, HttpRequestBody body) {
            this.url = url;
            this.type = type;
            this.headers = headers;
            this.body = body;
        }

        @Override
        public String getUrl() {
            return url;
        }

        @Override
        public HttpMethodType getType() {
            return type;
        }

        @Override
        public Map<String, String> getHeaders() {
            return headers;
        }

        @Override
        public HttpRequestBody getBody() {
            return body;
        }
    }

    class Builder {

        private String user;
        private String password;
        private String host;
        private Integer port;
        private String urlPath;
        private HttpMethodType type;
        private Map<String, String> headers;
        private HttpRequestBody body;
        //private List<HttpRequestBodyPart> bodyParts;
        private Map<String, String> urlParams;

        private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
        private static final String DEFAULT_FILE_CONTENT_TYPE = "application/octet-stream";

        public Builder() {
            headers = new HashMap<>();
            //bodyParts = new LinkedList<>();
            urlParams = new HashMap<>();
        }

        public Builder setBasicAuthorization(String user, String password) {
            this.user = user;
            this.password = password;
            return this;
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setPort(Integer port) {
            this.port = port;
            return this;
        }

        public Builder setUrlPath(String urlPath) {
            this.urlPath = urlPath;
            return this;
        }

        public Builder setType(HttpMethodType type) {
            this.type = type;
            return this;
        }

        public Builder addHeaderParam(String name, String value) {
            headers.put(name, value);
            return this;
        }

        /*public Builder addFilePart(String name, File file) {
            return addFilePart(name, file, "application/octet-stream", null);
        }

        public Builder addFilePart(String name, File file, String contentType) {
            return addFilePart(name, file, contentType, null);
        }

        public Builder addFilePart(String name, File file, String contentType, Charset charset) {
            bodyParts.add(new SimpleHttpRequestBodyPart<>(name, HttpRequestBodyPart.PartType.FILE, file, contentType, charset));
            return this;
        }

        public Builder addStringPart(String name, String content) {
            return addStringPart(name, content, DEFAULT_CHARSET);
        }

        public Builder addStringPart(String name, String content, Charset charset) {
            bodyParts.add(new SimpleHttpRequestBodyPart<>(name, HttpRequestBodyPart.PartType.STRING, content, null, charset));
            return this;
        }*/

        public Builder setFileBody(String fileName, File file) {
            return setFileBody(fileName, file, DEFAULT_FILE_CONTENT_TYPE);
        }

        public Builder setFileBody(String fileName, File file, String contentType) {
            body = new HttpRequestBody.DefaultImpl(HttpRequestBody.Type.FILE, file);
            headers.put(HttpHeader.CONTENT_TYPE, contentType);
            return this;
        }


        public Builder setJsonBody(JSONObject json) {
            return setJsonBody(json.toString());
        }

        public Builder setJsonBody(String json) {
            return setJsonBody(json, DEFAULT_CHARSET);
        }

        public Builder setJsonBody(JSONObject json, Charset charset) {
            return setJsonBody(json.toString(), charset);
        }

        public Builder setJsonBody(String json, Charset charset) {
            body = new HttpRequestBody.DefaultImpl(HttpRequestBody.Type.STRING, json);
            String header = "application/json";
            if (charset != null) {
                header += "; charset=" + charset.name().toLowerCase();
            }
            headers.put("Content-Type", header);
            return this;
        }

        public Builder addUrlParam(String name, String value) {
            urlParams.put(name, value);
            return this;
        }

        //todo добавить проверку на обязательные параметры
        public HttpRequest build() {
            if (user != null && !user.equals("") && password != null && !password.equals("")) {
                String authorization = "Basic " + Base64.getEncoder().encodeToString((user + ":" + password).getBytes());
                headers.put(HttpHeader.AUTHORIZATION, authorization);
            }
            String url = buildUrl();
            return new HttpRequest.DefaultImpl(url, type, headers, body);
        }

        private String buildUrl() {
            StringBuilder url = new StringBuilder();
            if (host != null) {
                url.append(host);
            }
            if (port != null) {
                if (url.toString().endsWith("/")) {
                    url = new StringBuilder(url.substring(0, url.lastIndexOf("/")) + ":" + port);
                } else {
                    url.append(":").append(port);
                }
            }
            if (urlPath != null) {
                if (urlPath.endsWith("/")) {
                    urlPath = urlPath.substring(0, urlPath.lastIndexOf("/"));
                }
                url.append(urlPath.startsWith("/") ? urlPath : "/" + urlPath);
            }
            if (urlParams.size() > 0) {
                url.append("?");
                for (String param : urlParams.keySet()) {
                    String value = urlParams.get(param).replaceAll(" ", "%20");
                    url.append(param).append("=").append(value).append("&");
                }
            }
            return url.toString();
        }
    }

}
