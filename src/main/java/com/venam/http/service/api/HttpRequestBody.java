package com.venam.http.service.api;

public interface HttpRequestBody {

    enum Type {
        FILE,
        STRING
    }

    Type getType();

    Object getContent();

    class DefaultImpl implements HttpRequestBody {

        private Type type;
        private Object content;

        public DefaultImpl(Type type, Object content) {
            this.type = type;
            this.content = content;
        }

        @Override
        public Type getType() {
            return type;
        }

        @Override
        public Object getContent() {
            return content;
        }
    }
}
