package com.venam.http.service.api;

import java.nio.charset.Charset;

public interface HttpRequestBodyPart<CONTENT> {

    HttpRequestBody.Type getType();

    String getName();

    CONTENT getContent();

    String getContentType();

    Charset getCharset();

    class DefaultImpl<CONTENT> implements HttpRequestBodyPart<CONTENT> {

        private String name;
        private HttpRequestBody.Type type;
        private CONTENT content;
        private String contentType;
        private Charset charset;

        public DefaultImpl(String name, HttpRequestBody.Type type, CONTENT content, String contentType, Charset charset) {
            this.name = name;
            this.type = type;
            this.content = content;
            this.contentType = contentType;
            this.charset = charset;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public HttpRequestBody.Type getType() {
            return type;
        }

        @Override
        public CONTENT getContent() {
            return content;
        }

        @Override
        public String getContentType() {
            return contentType;
        }

        @Override
        public Charset getCharset() {
            return charset;
        }
    }

}
