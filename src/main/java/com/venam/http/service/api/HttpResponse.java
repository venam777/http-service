package com.venam.http.service.api;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Http-ответ
 */
public interface HttpResponse {

    Integer getStatusCode();

    String getStatusText();

    String getBody();

    JSONObject getBodyJson() throws JSONException;

    class DefaultImpl implements HttpResponse {

        private Integer statusCode;
        private String statusText;
        private String body;

        public DefaultImpl(Integer statusCode, String statusText, String body) {
            this.statusCode = statusCode;
            this.statusText = statusText;
            this.body = body;
        }

        @Override
        public Integer getStatusCode() {
            return statusCode;
        }

        @Override
        public String getStatusText() {
            return statusText;
        }

        @Override
        public String getBody() {
            return body;
        }

        @Override
        public JSONObject getBodyJson() throws JSONException {
            return body != null && !body.equals("") ? new JSONObject(body) : null;
        }
    }
}
