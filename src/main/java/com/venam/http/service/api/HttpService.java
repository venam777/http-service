package com.venam.http.service.api;

import com.venam.http.service.util.ExceptionableCallback;

/**
 * Сервис для отправки http-запросов
 */
public interface HttpService {

    /**
     * Отправляет синхронный запрос, используя параметры соединения по умолчанию
     * @param request запрос
     * @return ответ
     * @throws HttpRequestException
     */
    HttpResponse sendSyncRequest(HttpRequest request) throws HttpRequestException;

    /**
     * Отправляет синхронный запрос, используя переданные параметры соединения
     * @param request запрос
     * @param connectionParams параметры соединения
     * @return Http-ответ
     * @throws HttpRequestException
     */
    HttpResponse sendSyncRequest(HttpRequest request, HttpConnectionParams connectionParams) throws HttpRequestException;

    /**
     * Отправляет асинхронный запрос, используя параметры соединения по умолчанию
     * @param request запрос
     * @param callback колбек для обработки ответа. В случае возникновения ошибки у callback'а вызовется метод onException
     * @return Http-ответ
     * @throws HttpRequestException
     */
    void sendAsyncRequest(HttpRequest request, ExceptionableCallback<HttpResponse, HttpRequestException> callback);

    /**
     * Отправляет асинхронный запрос, используя переданные параметры соединения
     * @param request запрос
     * @param connectionParams параметры соединения
     * @param callback колбек для обработки ответа. В случае возникновения ошибки у callback'а вызовется метод onException
     * @return Http-ответ
     * @throws HttpRequestException
     */
    void sendAsyncRequest(HttpRequest request, HttpConnectionParams connectionParams, ExceptionableCallback<HttpResponse, HttpRequestException> callback);

    /**
     * Устанавливает параметры соединения, которые будут по умолчанию использоваться для отправки запросов
     * @param connectionParams
     */
    void setDefaultConnectionParams(HttpConnectionParams connectionParams);

}
