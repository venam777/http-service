package com.venam.http.service.api;

public enum HttpMethodType {

    GET,
    POST,
    PUT,
    DELETE

}
