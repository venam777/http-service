package com.venam.http.service.api;

public class HttpHeader {

    public static final String CACHE_CONTROL = "Cache-Control";
    public static final String CONTENT_LENGTH = "Content-Length";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String DATE = "Date";
    public static final String PRAGMA = "Pragma";
    public static final String VIA = "Via";
    public static final String WARNING = "Warning";
    public static final String ACCEPT = "Accept";
    public static final String ACCEPT_CHARSET = "Accept-Charset";
    public static final String ACCEPT_ENCODING = "Accept-Encoding";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";
    public static final String ACCESS_CONTROL_REQUEST_HEADERS = "Access-Control-Request-Headers";
    public static final String ACCESS_CONTROL_REQUEST_METHOD = "Access-Control-Request-Method";
    public static final String AUTHORIZATION = "Authorization";
    public static final String CONNECTION = "Connection";
    public static final String COOKIE = "Cookie";
    public static final String EXPECT = "Expect";
    public static final String FROM = "From";

}
