package com.venam.http.service.api;

/**
 * Параметры http-подключения.
 * Здесь указываются именно параметры подключения, НЕ запроса.
 * Например, таймайт соединения, таймаут запроса и т.д.
 */
public interface HttpConnectionParams {

    /**
     * @return таймаут соединения
     */
    Integer getConnectionTimeout();

    /**
     * @return таймаут запроса
     */
    Integer getRequestTimeout();

    class Builder {

        private class SimpleHttpConnectionParams implements HttpConnectionParams {

            Integer connectionTimeout;
            Integer requestTimeout;

            private SimpleHttpConnectionParams() {}

            @Override
            public Integer getConnectionTimeout() {
                return connectionTimeout;
            }

            @Override
            public Integer getRequestTimeout() {
                return requestTimeout;
            }
        }

        private SimpleHttpConnectionParams connectionParams;

        public Builder() {
            connectionParams = new SimpleHttpConnectionParams();
        }

        public Builder setConnectionTimeout(Integer connectionTimeout) {
            this.connectionParams.connectionTimeout = connectionTimeout;
            return this;
        }

        public Builder setRequestTimeout(Integer requestTimeout) {
            this.connectionParams.requestTimeout = requestTimeout;
            return this;
        }

        public HttpConnectionParams build() {
            return connectionParams;
        }
    }

}
