package com.venam.http.service.util;

public interface ExceptionableCallback<P, E extends Exception> {

    void run(P param);

    void onException(E e);

    class DefaultImpl<P, E extends Exception> implements ExceptionableCallback<P, E> {

        @Override
        public void run(P param) {}

        @Override
        public void onException(E e) {}
    }

}
